FROM jenkins/jenkins:lts

# Install plugins
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt

# Copy Jenkins config as code
ENV CASC_JENKINS_CONFIG /var/jenkins_home/casc_configs/
COPY casc_configs/* ${CASC_JENKINS_CONFIG}

# Set java options.
ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false
